package ar.utn.dds.labo01;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class UnaClaseTest {

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	private UnaClase unaClase;

	@Before
	public void setUp() throws Exception {
		unaClase = new UnaClase();
		unaClase.agregarItem(new OtraClase("1", 2));
	}

	@Test
	public void aumentarNumero() {
		unaClase.aumentarNumero();
		assertEquals(3, unaClase.getUnListado().get(0).getNumero());
	}

	@Test
	public void agregarRepetido() throws Exception {

		thrown.expect(RepetidoException.class);
		unaClase.agregarItem(new OtraClase("1", 2));

	}
}
